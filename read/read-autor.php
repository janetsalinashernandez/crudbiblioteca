<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Informacion de Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id_autor= $_GET['id_autor'];

  if (empty($id_autor)) {
?>
  <p>Error, no se ha indicado el ID del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id_autor."';";

    $libro = pg_query($query) or die('La consulta fallo�: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algun autor con ID <?php echo $id_autor; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];
?>
<table>
  <caption>Informacion de autor</caption>
  <tbody>
    <tr>
      <th>ID AUTOR</th>
      <td><?php echo $id_autor; ?></td>
    </tr>
    <tr>
      <th>NOMBRE AUTOR</th>
      <td><?php echo $nombre_autor; ?></td>
    </tr>
    <tr>
      <th>LIBRO/S</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro_autor as la
        inner join biblioteca.autor as a
          on (la.id_autor = a.id_autor and la.id_autor = '".$id_autor."');";
            
	  $libros = pg_query($query) or die('La consulta fallo: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>  
      <p>Sin libros</p>
	  <?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
        <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de autores</a></li>
</ul>

</body>
</html>