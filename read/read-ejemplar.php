<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Informacion de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn= $_GET['isbn'];

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado la clave del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar,conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $ejemplar = pg_query($query) or die('La consulta fallo: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algun ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $clave_ejemplar = $tupla['clave_ejemplar'];
?>
<table>
  <caption>Informacion de ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE DE EJEMPLAR</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>CONSERVACION</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>TITULO</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro as li
        inner join biblioteca.libro_autor as la
          on (li.isbn = la.isbn and la.isbn = '".$isbn."');";

      $libros = pg_query($query) or die('La consulta fallo: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin titulo de libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.autor as a
        inner join biblioteca.libro_autor as la
          on (a.id_autor = la.id_autor and la.isbn = '".$isbn."');";
      

      $autores = pg_query($query) or die('La consulta fallo: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autores</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
                  <li><?php echo $atributo; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>