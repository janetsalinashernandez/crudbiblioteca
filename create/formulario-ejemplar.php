<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-ejemplar.php" method="post">
<table>
  <caption>Informacion de ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE DE EJEMPLAR: </th>
      <td><input type="text" name="clave_ejemplar" /></td>
    </tr>
    <tr>
      <th>ESTADO DE CONSERVACION</th>
      <td><textarea name="conservacion"></textarea></td>
    </tr>
	<tr>
      <th>ISBN</th>
      <td><textarea name="isbn"></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>