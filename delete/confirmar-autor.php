<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de AUTOR</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id_autor = $_GET['id_autor'];
  $error = false;
  if (empty($id_autor)) {
    $error = true;
?>
  <p>Error, no se ha indicado el ID del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id_autor."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
      $error = true;
?>
  <p>No se ha encontrado ningun autor con ID <?php echo $id_autor; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];
?>
<table>
  <caption>Información del autor</caption>
  <tbody>
    <tr>
      <th>ID AUTOR</th>
      <td><?php echo $id_autor; ?></td>
    </tr>
    <tr>
      <th>NOMBRE AUTOR</th>
      <td><?php echo $nombre_autor; ?></td>
    </tr>
    <tr>
      <th>Libro/s</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro_autor as la
        inner join biblioteca.libro as li
          on (la.id_libro = li.id_libro and la.id_autor = '".$id_autor."');";

      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-autor.php" method="post">
  <input type="hidden" name="id_autor" value="<?php echo $id_autor; ?>" />
  <p>¿Está seguro/a de eliminar  a este autor?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos los ejemplares en existencia de este autor.
  </p>
</form>

<form action="autores.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de autores</a></li>
</ul>

</body>
</html>