<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de EJEMPLAR</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $isbn = $_POST['isbn'];
  if (empty($isbn)) {
?>
  <p>Error, no se indico el ISBN del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar,conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado ningun ejemplar  con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $clave_ejemplar= $tupla['clave_ejemplar'];
      $conservacion_ejemplar= $tupla['conservacion_ejemplar'];
	  
      $query = "delete from biblioteca.ejemplar where isbn = '".$isbn."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      $query = "delete from biblioteca.libro_autor where isbn = '".$isbn."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      $query = "delete from biblioteca.libro where isbn = '".$isbn."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de borrar el ejemplar</p>
<?php
      } else {
?>
  <p>El ejemplar con ISBN <?php echo $isbn; ?> y titulo "<?php echo $titulo_libro; ?>" fue borrado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>