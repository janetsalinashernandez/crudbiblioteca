<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];
  $error = false;
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se ha indicado el ISBN del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar,conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $ejemplar= pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
      $error = true;
?>
  <p>No se ha encontrado ningun ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar= $tupla['conservacion_ejemplar'];
?>
<table>
  <caption>Información de ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE EJEMPLAR</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>ESTADO DE CONSERVACION</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
	 <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Libro/s</th>
      <td>
<?php
      $query = "select nombre_libro
        from biblioteca.libro_autor as la
        inner join biblioteca.libro as l
          on (la.id_autor = l.id_autor and la.isbn = '".$isbn."');";

      $ejemplares= pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="isbn" value="<?php echo $isbn; ?>" />
  <p>¿Está seguro/a de eliminar este ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos los ejemplares en existencia, así como la relación
    que mantenga con sus autores.
  </p>
</form>

<form action="ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de EJEMPLARES</a></li>
</ul>

</body>
</html>