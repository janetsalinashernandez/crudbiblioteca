<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Listado de EJEMPLARES</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<?php
  $servidorbd = "localhost";
  $nombrebd = "prueba";
  $usuariobd= "programador";
  $contraseniabd = "12345";

  $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
  or die('No se ha podido conectar: ' . pg_last_error());

  $query = 'select clave_ejemplar, conservacion_ejemplar, isbn from biblioteca.ejemplar';

  $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
?>

<body>
<table>
  <caption>Listado de EJEMPLARES</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>CLAVE_EJEMPLAR</th>
      <th>ESTADO DE CONSERVACION</th>
      <th>ISBN</th>
	  <th>Opción</th>
    </tr>
  </thead>
  <tbody>

<?php
  $contador = 1;
  while ($tupla = pg_fetch_array($resultado, null, PGSQL_ASSOC)) {
    $isbn = $tupla['isbn'];
?>
    <tr>
      <td>
        <?php echo $contador++; ?>
      </td>
<?php
    foreach ($tupla as $atributo) {
?>
      <td><?php echo trim($atributo); ?></td>
<?php
    }
?>
      <td>
        <a href="confirmar-ejemplar.php?isbn=<?php echo $isbn; ?>">Eliminar Ejemplar</a>
      </td>
    </tr>
<?php
  }

  pg_free_result($result);
  pg_close($dbconn);
?>

  </tbody>
</table>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>