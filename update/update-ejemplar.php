<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $isbn = $_POST['isbn'];
  $clave_ejemplar= $_POST['clave_ejemplar'];
  $conservacion_ejemplar= $_POST['conservacion_ejemplar'];
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN del ejemplar</p>
<?php
  }
  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se indico la clave del ejemplar</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado ningun ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $query = "update biblioteca.ejemplar
        set conservacion_ejemplar = '".$conservacion_ejemplar."'
              where isbn = '".$isbn."';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del ejemplar</p>
<?php
      } else {
?>
  <p>Los datos del ejemplar con ISBN <?php echo $isbn; ?> han sido actualizados con exito</p>
<?php
      }
    }
  }
?>
