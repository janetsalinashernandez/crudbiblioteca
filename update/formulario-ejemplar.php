<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de ejemplares</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado el ISBN del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $ejemplar= pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $conservacion_ejemplar= $tupla['conservacion_ejemplar'];
?>
<form action="update-ejemplar.php" method="post">
<table>
  <caption>Información del ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE EJEMPLAR</th>
      <td><input type="text" name="clave_ejemplar" value="<?php echo $clave_ejemplar; ?>" /></td>
    </tr>
    <tr>
      <th>ESTADO CONSERVACION</th>
      <td><textarea name="conservacion_ejemplar"><?php echo $conservacion_ejemplar; ?></textarea></td>
    </tr>
	   <tr>
      <th>ISBN</th>
      <td><textarea name="isbn"><?php echo $isbn; ?></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>